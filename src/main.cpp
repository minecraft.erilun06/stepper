#include <Arduino.h>

// first, include the library :)

#include <CheapStepper.h>
int pot=0;
int val = 0;

CheapStepper stepper;
// here we declare our stepper using default pins:
// arduino pin <--> pins on ULN2003 board:
// 8 <--> IN1
// 9 <--> IN2
// 10 <--> IN3
// 11 <--> IN4

// let's create a boolean variable to save the direction of our rotation

boolean moveClockwise = true;

void clockwise(int deg) {
	// let's do a clockwise move first
	moveClockwise = true;
	// let's move the stepper clockwise to position 2048
	// which is 180 degrees, a half-turn (if using default of 4096 total steps)
	stepper.moveTo (moveClockwise, 2048);
	// now let's print the stepper position to the console
	Serial.print("step position: ");
	Serial.print(stepper.getStep()); // get the current step position
	Serial.print(" / 4096");
	Serial.println();
}
void counterClockwise(int deg) {
	moveClockwise = false;
	stepper.moveTo (moveClockwise, 2048);
	Serial.print("step position: ");
	Serial.print(stepper.getStep());
	Serial.print(" / 4096");
	Serial.println();
}
void startpos() {
	moveClockwise = false;
	stepper.moveToDegree (moveClockwise, 0);
	Serial.print("step position: ");
	Serial.print(stepper.getStep());
	Serial.print(" / 4096");
	Serial.println();
}
void cw(int deg)
{
	clockwise(deg);
}

void ccw(int deg)
{
	counterClockwise(deg);
}



void setup()
{
	pinMode(pot,INPUT);
	// let's set a custom speed of 20rpm (the default is ~16.25rpm)
	stepper.setRpm(20);
	/* Note: CheapStepper library assumes you are powering your 28BYJ-48 stepper
	* using an external 5V power supply (>100mA) for RPM calculations
	* -- don't try to power the stepper directly from the Arduino
	* accepted RPM range: 6RPM (may overheat) - 24RPM (may skip)
	* ideal range: 10RPM (safe, high torque) - 22RPM (fast, low torque)
	*/
	// now let's set up a serial connection and print some stepper info to the console
	Serial.begin(9600); Serial.println();
	Serial.print(stepper.getRpm()); // get the RPM of the stepper
	Serial.print(" rpm = delay of ");
	Serial.print(stepper.getDelay()); // get delay between steps for set RPM
	Serial.print(" microseconds between steps");
	Serial.println();
	// stepper.setTotalSteps(4076);
	/* you can uncomment the above line if you think your motor
	* is geared 63.68395:1 (measured) rather than 64:1 (advertised)
	* which would make the total steps 4076 (rather than default 4096)
	* for more info see: http://forum.arduino.cc/index.php?topic=71964.15
	*/
	moveClockwise = true;
}

void loop()
{
	val = analogRead(pot);     // read the input pin
  Serial.println(val);             // debug value
	stepper.moveTo (moveClockwise, val);
	delay(1000);
}
